#!/bin/bash

source $HOME/.mya/config/config.cfg

name=$(uname -n)
echo Current user is: $name


if  [[ $name == "x230" ]]; then
    currentshow=$HOME/sync/oma/scripts/x230mya/settings/currentshow.txt
elif [[ $name == "elementary-ThinkPad-W520" ]]; then
    currentshow=$HOME/sync/oma/scripts/w520mya/settings/currentshow.txt
else
    currentshow=$currentshow
fi

filefolder=$(cat $currentshow)/

grep=$(echo ${filefolder#*hdd/})
grep2=${grep%?}

echo -e "\e[1;31mThis script easily allows you to watch $grep2 and mpv deals with bookmarking\e[0m"
echo
mpv $filefolder
