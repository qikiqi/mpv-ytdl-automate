#!/bin/bash

# TODO Create more versatile renamer / illegalcharacter remover

source $HOME/.mya/config/config.cfg

name=$(uname -n)
echo Current user is: $name


if  [[ $name == "x230" ]]; then
    filefolder2=$HOME/hon/
    formatfile=$HOME/sync/oma/scripts/x230mya/settings/honrenamerformatfile.txt
    characterstoremove=$HOME/sync/oma/scripts/x230mya/settings/honrenamercharsremov.txt
elif [[ $name == "elementary-ThinkPad-W520" ]]; then
    filefolder2=$HOME/hdd/hon/
    formatfile=$HOME/sync/oma/scripts/w520mya/settings/honrenamerformatfile.txt
    characterstoremove=$HOME/sync/oma/scripts/w520mya/settings/honrenamercharsremov.txt
else
    filefolder2=$filefolder2
    formatfile=$formatfile
    characterstoremove=$characterstoremove
fi

startmenu=$startmenu
renumber=$renumber
formatfile=$(cat $formatfile)
charemov=$(cat $characterstoremove)

echo "This script removes the autonumbering from videos downloaded with hondwnldr"
echo "Are you sure you want to remove first 7 characters of the whole honfolder?"
echo "PREVIEW:"
for file in $filefolder2/*.$formatfile
do
    num=${#filefolder2}
    num=$((num+2))
    file=$(echo $file | cut -c $num-)
    newname=$(echo $file | cut -c $characterstoremove-)
    echo "$filefolder2$file" "$filefolder2$newname"
done

echo "Type 'ok' if you want to rename, anything else to exit"
read sure
if [[ $sure == "ok" ]]; then
    for file in $filefolder2/*.$formatfile
    do
        num=${#filefolder2}
        num=$((num+2))
        file=$(echo $file | cut -c $num-)
        newname=$(echo $file | cut -c $characterstoremove-)
        mv "$filefolder2$file" "$filefolder2$newname"
    done
    $renumber
else
    echo "Exiting..."
    $startmenu
fi
