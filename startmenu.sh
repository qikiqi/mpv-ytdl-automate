#!/bin/bash
# TODO Add settings where you can change the location of txt files
# TODO Make a tool that lets you download the same way as you listed and download from history would be nice
# TODO Create an installer that "installs" this script
# TODO Search tool - possibility to search automatically from your history based on search terms - then start playing is 1 found, if multiple show all and let user select
# TODO Add possibility to settings to change mpv autoskipper tags
# TODO Make a tool that removes all "illegal" characters from folder(s)/file(s)
# TODO Make different possibilities for colors of neo-dadaism
# TODO Every single user check must be changed for support of unlimited users
# TODO Add updater to settings, can check what is the status of the git and updates if needed


# TODO 0. Make copy that will be modified
# TODO 1. Delete actual command ( awk 'BEGIN{FS=OFS="|"} NF>1{$1="";sub(/^- */, "")}'1 onlinehistorycolour.txt > lol.txt )
# TODO 2. Add some character before every file ( sed -e 's/^/!/' lol.txt > lol2.txt)
# TODO 3. Replace !| with \e[1;55m ( sed -r 's/\!\|/\\e[1;55m/g' lol2.txt > lol3.txt)
# TODO 4. Replace | with \e[0m ( sed -r 's/\|/\\e[0m/g' lol3.txt > lol4.txt )
# TODO 5. Add some character in the end of every line ( sed -e 's/$/ @+@/' -i lol4.txt )
# TODO 6. Replace just added character with \n while printing it with echo -e ( echo -e $(cat lol4.txt | tr '@+@' '\n' )

# First we test which computer is using this script so we can determine the right folders for history, content etc.

source $HOME/.mya/config/config.cfg

# Create better randomizer with multiple variables and we randomly assign them different spots etc
rnumber=$((RANDOM%10))
case "$selection" in

1) echo -e "mpv-ytdl-automate --- NEEDS MORE NEO-DADAISM"
    ;;
2) echo -e "mpv-ytdl-automate --- NEEDS MORE NEO-DADAISM"
    ;;
3) echo -e "mpv-ytdl-automate --- NEEDS MORE NEO-DADAISM"
    ;;
4) echo -e "mpv-ytdl-automate --- NEEDS MORE NEO-DADAISM"
   ;;
5) echo -e "mpv-ytdl-automate --- NEEDS MORE NEO-DADAISM"
   ;;
7) echo -e "mpv-ytdl-automate --- NEEDS MORE NEO-DADAISM"
   ;;
8) echo -e "mpv-ytdl-automate --- NEEDS MORE NEO-DADAISM"
   ;;
9) echo -e "mpv-ytdl-automate --- NEEDS MORE NEO-DADAISM"
   ;;
10) echo -e "mpv-ytdl-automate --- NEEDS MORE NEO-PEPES"
   ;;
esac


echo "mp-ytdl-automate --- NEEDS MORE NEO-DADAISM"
name=$(uname -n)
echo Current user is: $name
if  [[ $name == "x230" ]]; then
    onlinehistory=$HOME/sync/oma/scripts/x230mya/onlinehistory.txt
    offlinehistory=$HOME/sync/oma/scripts/x230mya/offlinehistory.txt
    tracker=$HOME/sync/oma/scripts/x230mya/track/count.txt
    trackset=$HOME/sync/oma/scripts/x230mya/settings/trackset.txt
elif [[ $name == "elementary-ThinkPad-W520" ]]; then
    onlinehistory=$HOME/sync/oma/scripts/w520mya/onlinehistory.txt
    offlinehistory=$HOME/sync/oma/scripts/w520mya/offlinehistory.txt
    tracker=$HOME/sync/oma/scripts/w520mya/track/count.txt
    trackset=$HOME/sync/oma/scripts/w520mya/settings/trackset.txt
else
    onlinehistory=$onlinehistory
    offlinehistory=$offlinehistory
    tracker=$tracker
    trackset=$trackset
fi

offlineplayer=$offlineplayer
onlineplayer=$onlineplayer
watchshow=$watchshow
historyviewer=$historyviewer
hondownloader=$hondownloader
listformat=$listformat
playliststart=$playliststart
honrenamer=$honrenamer
settings=$settings

# TODO This throws an error if file hasn't been created yet, fix via checker if-else
trackset2=$(cat $trackset)

if [[ $trackset2 == "1" ]]; then
    currentamount=$(cat $tracker)
    added=$(($currentamount+1))
    echo $added > $tracker
    echo "You have used this script $added times"
fi

# Reads users selection
echo "----------"
echo -n -e "\e[1;55m Players \e[0m \n\e[1;31m[ Online\t= 1 ] \n[ Offline\t= 2 ]\n\e[0m\n\e[1;55m History \e[0m \n\e[1;31m[ Online \t= 3 ]\n[ Offline \t= 4 ]\e[0m \n \e[1;55m\n Latest item from history \e[0m \n\e[1;31m[ Online \t= 5 ] \n[ Offline \t= 6 ]\e[0m \n \t \e[1;55m \nVarious Tools \e[0m \n\e[1;31m[ /a/ \t\t= 7 ] \n[ Hondwnldr \t= 8 ]\e[0m \e[0m \n\e[1;31m[ List-format \t= 9 ] \n[ Playlists \t= q ]\e[0m \n\e[1;31m[ Honrenamer \t= w ] \n[ Settings \t= e ]\e[0m"

echo
read -n 1 selection
echo
echo
case "$selection" in

1)  echo "Online"
    $onlineplayer
    ;;
2)  echo  "Offline"
    $offlineplayer
    ;;
3)  echo  "Complete online history"
    $historyviewer $onlinehistory
    ;;
4) echo  "Complete offline history"
    $historyviewer $offlinehistory
   ;;
5) echo "Last online content"
    $historyviewer $onlinehistory last
   ;;
7) echo  "/a/"
    $watchshow
   ;;
8) echo "Hondownloader"
    $hondownloader
   ;;
9) echo "List formats"
    $listformat
   ;;
q) echo "Playlists"
    $playliststart
   ;;
w) echo "Honrenamer"
    $honrenamer
   ;;
e) echo "Settings"
    $settings
    ;;
*) echo "Last offline content"
    $historyviewer $offlinehistory last
   ;;
esac
