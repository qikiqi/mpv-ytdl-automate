#!/bin/bash

source $HOME/.mya/config/config.cfg

name=$(uname -n)
echo Current user is: $name
if  [[ $name == "x230" ]]; then
    trackset=$HOME/sync/oma/scripts/x230mya/settings/trackset.txt
    formatfile=$HOME/sync/oma/scripts/x230mya/settings/honrenamerformatfile.txt
    timeoutfile=$HOME/sync/oma/scripts/x230mya/settings/hondownloadertimeoutfile.txt
    currentshow=$HOME/sync/oma/scripts/x230mya/settings/currentshow.txt
    characterstoremove=$HOME/sync/oma/scripts/x230mya/settings/honrenamercharsremov.txt
elif [[ $name == "elementary-ThinkPad-W520" ]]; then
    trackset=$HOME/sync/oma/scripts/w520mya/settings/trackset.txt
    formatfile=$HOME/sync/oma/scripts/w520mya/settings/honrenamerformatfile.txt
    timeoutfile=$HOME/sync/oma/scripts/w520mya/settings/hondownloadertimeoutfile.txt
    currentshow=$HOME/sync/oma/scripts/w520mya/settings/currentshow.txt
    characterstoremove=$HOME/sync/oma/scripts/w520mya/settings/honrenamercharsremov.txt
else
    trackset=$trackset
    formatfile=$formatfile
    timeoutfile=$timeoutfile
    currentshow=$currentshow
    characterstoremove=$characterstoremove
fi

settings=$settings
startmenu=$startmenu


tracker=$(cat $trackset)
currentformat=$(cat $formatfile)
curtimeout=$(cat $timeoutfile)
curshow=$(cat $currentshow)
curremov=$(cat $characterstoremove)
digit='^[0-9]+$'

# Reads users selection
echo "Here you can change various settings of this toolkit"
echo "----------"
# TODO Add else here that creates the file if for some reason file cant be found
if [[ $tracker == "1" ]]; then
    echo -n -e "\e[1;55m Tracking \e[0m \n\e[1;31m[ On\t= 1 ]\e[0m \n[ Off\t= 2 ]"
elif [[ $tracker == "0" ]]; then
    echo -n -e "\e[1;55m Tracking \e[0m \n[ On\t= 1 ] \n\e[1;31m[ Off\t= 2 ]\e[0m"
fi

echo
echo -n -e "\n\e[1;55m Honrenamer extension from: $currentformat \e[0m \n\e[1;31m[ Change = 3 ]\e[0m"

echo
echo -n -e "\n\e[1;55m Honrenamer removes $curremov characters from start \e[0m \n\e[1;31m[ Change = 4 ]\e[0m"

echo
echo -n -e "\n\e[1;55m Hondwnldr timeout from: $curtimeout \e[0m \n\e[1;31m[ Change = 5 ]\e[0m"

echo
echo -n -e "\n\e[1;55m Folder of the current show\n Current folder: $curshow \e[0m \n\e[1;31m[ Select = 6 ]\e[0m"

echo
echo -n -e "\n\e[1;55m Main Menu \e[0m \n\e[1;31m[ Return = z ]\e[0m"

echo
read -n 1 selection
echo
echo

case "$selection" in

1)  echo "Turning tracking on"
    echo 1 > $trackset
    ;;
2)  echo  "Turning tracking off"
    echo 0 > $trackset
    ;;
3)  echo "Changing the honrenamer format"
    echo "Type the format of the files that honrenamer should look"
    echo "Write only the extension without dot (.) or anything else"
    read format
    echo $format > $formatfile
    ;;
4)  echo  "Changing the amount of characters to remove"
    read charmount
    if [[ $charmount =~ $digit ]]; then
        echo $charmount > $characterstoremove
    else
        echo -e "\e[0m \n\e[1;31mNot valid\e[0m, only numbers are accepted"
    fi
    ;;
5)  echo  "Changing the timeout"
    echo "Change the hondownloader timeout by typing new timeout"
    read usertimeout
    if [[ $usertimeout =~ $digit ]]; then
        echo $usertimeout > $timeoutfile
    else
        echo -e "\e[0m \n\e[1;31mNot valid\e[0m, only numbers are accepted"
    fi
    ;;
6)  echo "Selecting the folder of current show"
    show=$(dialog --title "Select folder to watch" --stdout --title "Please navigate yourself to FOLDER" --fselect $HOME 10 40)
    echo $show > $currentshow
    ;;
z)  echo  "To the main menu we go..!"
    $startmenu
    ;;
esac

$settings
