#!/bin/bash

# TODO Should this be added to history or not? No, but if you watch with it (when its possible), then it should be added
# TODO Make it recognize where the video is from and based on that make further questions


name=$(uname -n)
echo Current user is: $name
echo -e "\e[1;31mThis script allows you to list different possible formats of video \e[0m"
echo -n -e "Type the URL of the video then press \e[1;31m[ENTER]\e[0m: "
read ID
echo

youtube-dl --list-format $ID
echo -n -e "Type the video format then \e[1;31m[ENTER]\e[0m: "
read vidf
echo -n -e "Type the audio format then \e[1;31m[ENTER]\e[0m: "
read audf
mpv --ytdl-format=$vidf+$audf $ID
exit 1
