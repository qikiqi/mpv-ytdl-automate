#!/bin/bash

# TODO Possibility to remove playlists

source $HOME/.mya/config/config.cfg

playlistselector=$playlistselector
playlistmaker=$playlistmaker
remover=$remover
startmenu=$startmenu

# Reads users selection
echo "Welcome to playlists, here you can watch from playlist or create new playlists!"
echo "----------"
echo -n -e "\e[1;55m Playlist \e[0m \n\e[1;31m[ Selector\t= 1 ] \n[ Maker\t\t= 2 ]\n[ Remover\t= 3 ]\n[ Startmenu\t= 4 ]\e[0m"

echo
read -n 1 selection
echo
echo
case "$selection" in

1)  echo "Selector"
    $playlistselector
    ;;
2)  echo  "Maker"
    $playlistmaker
    ;;
3) echo "Remover"
    $remover
    ;;
4) echo "Startmenu"
    $startmenu
    ;;
esac
