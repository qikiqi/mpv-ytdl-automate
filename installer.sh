#!/bin/bash

# This will install mpv-ytdl-automate and its requirements
# if dependencies are not found, manual installation of those is advised

# Dependencies: mpv, youtube-dl, git

# TODO Create settings folders and reformat those withing all the code
# TODO Ask where we should place the settings folder (default should be .config)
# TODO Then we need to create a filelocationsettinglist where every single script gets its information for variables

# Check bash or fish

# TODO UNCOMMENT WHEN TESTING IS OVER
# sudo apt-get update

name=$(uname -n)
echo Current user is: $name

systembash=$(which bash)
systemfish=$(which fish)

if [[ ! $systembash == "" ]] | [[ ! $systemfish == "" ]]; then
    echo "Bash and fish found"
    read whichone
    systembash="1"
    systemfish="1"
elif [[ ! $systembash == "" ]] | [[ $systemfish == "" ]]; then
    echo "Bash found"
    systembash="1"
    systemfish="0"
elif [[ $systembash == "" ]] | [[ ! $systemfish == "" ]]; then
    echo "Fish found"
    systembash="0"
    systemfish="1"
elif [[ $systembash == "" ]] | [[ $systemfish == "" ]]; then
    echo "What shell are you using?"
    echo "Manually make permanent alias / function to this script within your shell"
    systembash="0"
    systemfish="0"
fi


systemmpv=$(which mpv)
if [[ $systemmpv == "" ]]; then
    echo "mpv not found"
    echo "It is needed to use this script"
    echo "Do you want to install mpv? [ Y / n ]"
    read mpvinstall
    if [[ $mpvinstall == "n" ]]; then
        exit 1
    else
        sudo apt-get install -y mpv
    fi
fi

systemyoutubedl=$(which youtube-dl)
if [[ $systemyoutubedl == "" ]]; then
    echo "youtube-dl not found"
    echo "It is needed to use this script"
    echo "Do you want to install it? [ Y / n ]"
    read youtubedlinstall
    if [[ $youtubedlinstall == "n" ]]; then
        exit 1
    else
        sudo apt-get install -y youtube-dl
    fi
fi

systemgit=$(which git)
if [[ $systemgit == "" ]]; then
    echo "git not found"
    echo "It is needed to use this script"
    echo "Do you want to install it? [ Y / n ]"
    read gitinstall
    if [[ $gitinstall == "n" ]]; then
        exit 1
    else
        sudo apt-get install -y git
    fi
fi

systemdialog=$(which dialog)
if [[ $systemdialog == "" ]]; then
    echo "dialog not found"
    echo "It is needed to use this script"
    echo "Do you want to install dialog? [ Y / n ]"
    read dialoginstall
    if [[ $dialoginstall == "n" ]]; then
        exit 1
    else
        sudo apt-get install -y dialog
    fi
fi


systemmpv=$(which mpv)
if [[ $systemmpv == "" ]]; then
    echo "mpv not found, try installing it by hand or from ppa"
    echo "Exiting..."
    exit 1
else
    echo "mpv found"
fi

systemyoutubedl=$(which youtube-dl)
if [[ $systemyoutubedl == "" ]]; then
    echo "youtube-dl not found, try installing it by hand or from ppa"
    echo "Exiting..."
    exit 1
else
    echo "youtube-dl found"
fi

systemgit=$(which git)
if [[ $systemgit == "" ]]; then
    echo "git not found, try installing it by hand or from ppa"
    echo "Exiting..."
    exit 1
else
    echo "git found"
fi

# TODO What to do if folder already exists? Check content and update if needed
mkdir $HOME/.mya
echo ".mya folder created"

while true
do
    # TODO There needs to be check what chcks that cloning was succesful
    git clone --depth=1 "https://bitbucket.org/qikiqi/mpv-ytdl-automate/" $HOME/.mya
    # TODO Remove README?? AND CONTRIBUTORS??
    if [[ -f "$HOME/.mya/startmenu.sh" ]]; then
        cd $HOME/.mya
        verifygit=$HOME/.mya/verifygit.txt
        verify=$(md5sum -c $verifygit --quiet)
        if [[ $verify == "" ]]; then
            echo "Corrupted files not found"
            rm -rf $HOME/.mya/.git
            break
        else
            echo "Could not verify the integrity of .sh files, trying again..."
        fi
    else
        echo ".sh not found"
    fi
done


if [[ $systemfish == "1" ]]; then
    echo "Do you wanna add function to fish? [ Y / n ]"
    read fishalias
    if [[ $fishalias == "n" ]]; then
        break
    else
        echo "Creating function to fish"
        echo "What the name of the function should be?"
        read called
        touch $HOME/.config/fish/functions/$called.fish
        echo "function $called" >> $HOME/.config/fish/functions/$called.fish
        echo "$HOME/.mya/startmenu.sh" >> $HOME/.config/fish/functions/$called.fish
        echo "end" >> $HOME/.config/fish/functions/$called.fish
    fi
fi

if [[ $systembash == "1" ]]; then
    echo "Do you wanna add alias to your .bashrc? [ Y / n ]"
    read bashalias
    if [[ $bashalias == "n" ]]; then
        break
    else
        echo "Creating permanent alias to bash"
        echo "What the name of the alias should be?"
        read called2
        echo "alias $called2='$HOME/.mya/startmenu.sh'" >> $HOME/.bashrc
    fi
fi

systemdialog=$(which dialog)
if [[ $systemdialog == "" ]]; then
    echo "dialog not found, you need to configure honfolders via \"hard way\""
fi

# TODO Add alternative method to dialogadders
# WRITE ASKER THAT ASKS WHERE HONVIDEOS ARE LOCATED $honfilelocation
echo "Selecting the folder of honvideos"
honfilelocation=$(dialog --title "Select folder where honvideos are located" --stdout --title "Please navigate yourself to honvideos folder" --fselect $HOME 14 48)
# VIA COOL FILESELECTOR???

# WRITE ASKER THAT ASKS WHERE HONVIDEOS ARE LOCATED $honfinalsfilelocation
honfinalsfilelocation=$(dialog --title "Select folder where honFINALSvideo are located" --stdout --title "Please navigate yourself to honFINALSvideo folder" --fselect $HOME 14 48)
# VIA COOL FILESELECTOR???

# WRITE ASKER THAT ASKS WHERE HONVIDEOS ARE LOCATED $honfinalsfilelocation
# VIA COOL FILESELECTOR???

# TODO This structure should only be applied when dealing with NEW computer
# TODO If else conditions for w520 and x230
if  [[ $name == "x230" ]]; then

location=$HOME/sync/oma/scripts/x230mya
scripts=$HOME/sync/oma/scripts/mpv-ytdl-automate

elif [[ $name == "elementary-ThinkPad-W520" ]]; then

location=$HOME/sync/oma/scripts/w520mya
scripts=$HOME/sync/oma/scripts/mpv-ytdl-automate

else

location=$HOME/.mya/config
scripts=$HOME/.mya

# TODO Possibility to set installation path wherever vai dialog?
# At the moment big part of the script relays on static installation folder
# If this is going to be implemented, move this to be the first thing

mkdir $location/
mkdir $location/settings
mkdir $location/playlists
config=$location/config.cfg

fi

#prompt
echo "savefile=\"$location/onlinehistory.txt\"" > $config
echo "twitchsavefile=\"$location/twitchhistory.txt\"" >> $config
echo "honplayer=\"$scripts/honplayer.sh\"" >> $config

#honplayer
echo "startmenu=\"$scripts/startmenu.sh\"" >> $config
echo "filefolder=\"$honfilelocation*\"" >> $config
echo "finalfolder2=\"$honfinalsfilelocation*\"" >> $config # honfinalsfilelocation2 not needed cuz the only difference is star (*)
echo "finalfolder=\"$honfinalsfilelocation\"" >> $config
echo "honsavefile=\"$location/offlinehistory.txt\"" >> $config

#startmenu
echo "settings=\"$scripts/settings.sh\"" >> $config
echo "honrenamer=\"$scripts/honrenamer.sh\"" >> $config
echo "playliststart=\"$scripts/playliststart.sh\"" >> $config
echo "listformat=\"$scripts/listformat.sh\"" >> $config
echo "hondownloader=\"$scripts/hondownloader.sh\"" >> $config
echo "historyviewer=\"$scripts/historytool.sh\"" >> $config
echo "watchshow=\"$scripts/serieswatcher.sh\"" >> $config
echo "onlineplayer=\"$scripts/prompt.sh\"" >> $config
echo "offlineplayer=\"$scripts/honplayer.sh\"" >> $config
echo "trackset=\"$location/settings/trackset.txt\"" >> $config
echo "tracker=\"$location/settings/count.txt\"" >> $config
echo "offlinehistory=\"$location/offlinehistory.txt\"" >> $config
echo "onlinehistory=\"$location/onlinehistory.txt\"" >> $config

#historytool
echo "offlinehistorytemp=\"$location/offlinehistorytemp.txt\"" >> $config
echo "offlinehistorycolour=\"$location/offlinehistorycolour.txt\"" >> $config
echo "onlinehistorytemp=\"$location/onlinehistorytemp.txt\"" >> $config
echo "onlinehistorycolourfinal=\"$location/finalonlinehistorycolour.txt\"" >> $config
echo "onlinehistorycolour=\"$location/onlinehistorycolour.txt\"" >> $config

#hondownloader
echo "timeoutfile=\"$location/settings/hondownloadertimeoutfile.txt\"" >> $config
echo "filefolder2=\"$honfilelocation\"" >> $config

#honrenamer
echo "renumber=\"$scripts/honrenumber.sh\"" >> $config
echo "characterstoremove=\"$location/settings/honrenamercharsremov.txt\"" >> $config
echo "formatfile=\"$location/settings/honrenamerformatfile.txt\"" >> $config

#honrenumber
# NEED TO CHANGE IN SCRIPT FILEFOLDER >> FILEFOLDER2 CUZ STAR IS NOT NEEDED / WANTED

#serieswatcher
echo "currentshow=\"$location/settings/currentshow.txt\"" >> $config

#playliststart
echo "remover=\"$scripts/playlistremover.sh\"" >> $config
echo "playlistmaker=\"$scripts/playlistmaker.sh\"" >> $config
echo "playlistselector=\"$scripts/playlistselector.sh\"" >> $config

#playlistmaker
# NEED TO CHANGE IN SCRIPT playlistfolder >> playlistfolder2 CUZ STAR IS NOT NEEDED / WANTED
echo "playlistfolder2=\"$location/playlists\"" >> $config

#playlistselector
echo "playlistfolder=\"$location/playlists/*\"" >> $config

#playlistremover

#settings
echo "characterstoremove=\"$location/settings/characterstoremove.txt\"" >> $config



echo "1" > $location/settings/trackset.txt
touch $location/settings/currentshow.txt
touch $location/settings/hondownloadertimeoutfile.txt
touch $location/settings/honrenamerformatfile.txt
touch $location/settings/honrenamercharsremov.txt
touch $location/settings/characterstoremove.txt
touch $location/onlinehistory.txt
touch $location/offlinehistory.txt
touch $location/finalonlinehistorycolour.txt
touch $location/count.txt
touch $location/twitchhistory.txt


# TODO Check if this file matches and is correctly formatted according everysinglevariabledir
# Then run this installer in virtualmachine and lets see what happens
# and then make a copy of mpv-ytdl-automate and start changing the dir in that copy
# Then test again in virtual
# TODO Add couple questions 
# TODO Settings need some files to be created already, create here so no errors present when using settings
