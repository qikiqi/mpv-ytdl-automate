#!/bin/bash

# This is a script that helps keep history what content you have been watching
# Known bugs: Cant handle |, { and *. Removed in prompt.sh

# TODO Add functionality which checks the health of history file and tries to correct if errors are found. NOT NEEDED IF ERRORS ARE NOT MADE (but is this possible, misclick or something could create them)
# TODO Here is some error that tries to play whole name instead link (REMEMBER TO GREP ONLY THE NAME OF THE FILE, NOT WHOLE SAVEFILELINE) <-- that is the problem lol

# 0. Make copy that will be modified
# 1. Delete actual command ( awk 'BEGIN{FS=OFS="|"} NF>1{$1="";sub(/^- */, "")}'1 onlinehistorycolour.txt > lol.txt )
# 2. Add some character before every file ( sed -e 's/^/!/' lol.txt > lol2.txt)
# 3. Replace !| with \e[1;55m ( sed -r 's/\!\|/\\e[1;55m/g' lol2.txt > lol3.txt)
# 4. Replace | with \e[0m ( sed -r 's/\|/\\e[0m/g' lol3.txt > lol4.txt )
# 5. Add some character in the end of every line ( sed -e 's/$/ @+@/' -i lol4.txt )
# 6. Replace just added character with \n while printing it with echo -e ( echo -e $(cat lol4.txt | tr '@+@' '\n'echo -e $(cat lol4.txt | tr '@+@' '\n' )

# Replace numbering with , ( sed -e 's/000[[:digit:]]\+\-/\,/g' offlinehistory.txt > $somethi )
# Replace everything until , with colour start ( sed -e 's/.*,/\\e[1;31m/g' -i $somethi )
# Replace newlines with @ ( sed -e 's/$/ {@/' -i $somethi )
# Replace { with colour end ( sed -r 's/\{/\\e[0m/g' -i $somethi  )
# Replace @ with newline ( )

source $HOME/.mya/config/config.cfg

name=$(uname -n)
echo Current user is: $name
if  [[ $name == "x230" ]]; then
    onlinehistorycolour=$HOME/sync/oma/scripts/x230mya/onlinehistorycolour.txt
    onlinehistorycolourfinal=$HOME/sync/oma/scripts/x230mya/finalonlinehistorycolour.txt
    onlinehistorytemp=$HOME/sync/oma/scripts/x230mya/onlinehistorytemp.txt
    onlinehistory=$HOME/sync/oma/scripts/x230mya/onlinehistory.txt
    offlinehistorycolour=$HOME/sync/oma/scripts/x230mya/offlinehistorycolour.txt
    offlinehistorytemp=$HOME/sync/oma/scripts/x230mya/offlinehistorytemp.txt
    offlinehistory=$HOME/sync/oma/scripts/x230mya/offlinehistory.txt
elif [[ $name == "elementary-ThinkPad-W520" ]]; then
    onlinehistorycolour=$HOME/sync/oma/scripts/w520mya/onlinehistorycolour.txt
    onlinehistorycolourfinal=$HOME/sync/oma/scripts/w520mya/finalonlinehistorycolour.txt
    onlinehistorytemp=$HOME/sync/oma/scripts/w520mya/onlinehistorytemp.txt
    onlinehistory=$HOME/sync/oma/scripts/w520mya/onlinehistory.txt
    offlinehistorycolour=$HOME/sync/oma/scripts/w520mya/offlinehistorycolour.txt
    offlinehistorytemp=$HOME/sync/oma/scripts/w520mya/offlinehistorytemp.txt
    offlinehistory=$HOME/sync/oma/scripts/w520mya/offlinehistory.txt
else
    onlinehistorycolour=$onlinehistorycolour
    onlinehistorycolourfinal=$onlinehistorycolourfinal
    onlinehistorytemp=$onlinehistorytemp
    onlinehistory=$onlinehistory
    offlinehistorycolour=$offlinehistorycolour
    offlinehistorytemp=$offlinehistorytemp
    offlinehistory=$offlinehistory
fi

offlineplayer=$offlineplayer
onlineplayer=$onlineplayer

# This is for the case if startmenu.sh sent that user wants to see online history
# line what begins with awk colours videos title
# nl generates numbers in front of each line
# read the number of the file that user wants to watch and sed the nth line and play it
if [[ $2 == "playlist" ]]; then
    awk 'BEGIN{FS=OFS="|"} NF>1{$1="";sub(/^- */, "")}'1 $1 > $1.temp && sed -e 's/^/!/' -i $1.temp && sed -r 's/\!\|/\\e[1;31m/g' -i $1.temp && sed -r 's/\|/\\e[0m/g' -i $1.temp && sed -e 's/$/ {/' -i $1.temp && echo -e $(cat $1.temp) > $1.temp2 && cat $1.temp2 | tr '{' '\n' > $1.temp && sed -e 's/^[ \t]*//' -i $1.temp
    nl $1.temp
    echo -n -e "Select the file you want to watch by typing its number and then press \e[1;31m[ENTER]\e[0m"
    read input
    rm $1.temp
    rm $1.temp2
    $(sed "${input}q;d" $1)
fi

# This is for the case if startmenu.sh sent that user wants watch the latest video from history
# input is the number of lines in the file (=latest video)
# sed the last line and play it
if [[ $1 == $onlinehistory  ]] && [[ $2 == "last" ]]; then
    input=$(wc -l < $onlinehistory)
    $(sed "${input}q;d" $onlinehistory)
# TODO Go back to startmenu.sh?
fi

# This is for the case if startmenu.sh sent that user wants watch the latest video from history
# input is the number of lines in the file (=latest video)
# sed the last line and play it
if [[ $1 == $offlinehistory  ]] && [[ $2 == "last" ]]; then
    input=$(wc -l < $offlinehistory)
    mpv "$(sed "${input}q;d" $offlinehistory)"
    exit 1
# TODO Go back to startmenu.sh?
fi



# This is for the case if startmenu.sh sent that user wants to see online history
# line what begins with awk colours videos title
# nl generates numbers in front of each line
# read the number of the file that user wants to watch and sed the nth line and play it
if [[ $1 == $onlinehistory ]]; then
    awk 'BEGIN{FS=OFS="|"} NF>1{$1="";sub(/^- */, "")}'1 $onlinehistory > $onlinehistorycolourfinal && sed -e 's/^/!/' -i $onlinehistorycolourfinal && sed -r 's/\!\|/\\e[1;31m/g' -i $onlinehistorycolourfinal && sed -r 's/\|/\\e[0m/g' -i $onlinehistorycolourfinal && sed -e 's/$/ {/' -i $onlinehistorycolourfinal && echo -e $(cat $onlinehistorycolourfinal) > $onlinehistorytemp && cat $onlinehistorytemp | tr '{' '\n' > $onlinehistorycolourfinal && sed -e 's/^[ \t]*//' -i $onlinehistorycolourfinal
    nl $onlinehistorycolourfinal
    echo -n -e "Select the file you want to watch by typing its number and then press \e[1;31m[ENTER]\e[0m"
    read input
#TODO 
    $(sed "${input}q;d" $onlinehistory)
fi

# This is for the case if startmenu.sh sent that user wants to see online history
# nl generates numbers in front of each line
# read the number of the file that user wants to watch and sed the nth line and play it
if [[ $1 == $offlinehistory ]]; then
    sed -e 's/[[:digit:]]\+\-/\,/g' $offlinehistory > $offlinehistorycolour && sed -e 's/.*,/\\e[1;31m/g' -i $offlinehistorycolour && sed -e 's/$/ {@/' -i $offlinehistorycolour && sed -r 's/\{/\\e[0m/g' -i $offlinehistorycolour && echo -e $(cat $offlinehistorycolour) > $offlinehistorytemp && cat $offlinehistorytemp | tr '@' '\n' > $offlinehistorycolour && sed -e 's/^[ \t]*//' -i $offlinehistorycolour
    nl $offlinehistorycolour
    echo -n -e "Select the file you want to watch by typing its number and then press \e[1;31m[ENTER]\e[0m"
    read input
    mpv "$(sed "${input}q;d" $offlinehistory)"
fi
