#!/bin/bash

# TODO Possibility to watch whole playlist from beginning to end

source $HOME/.mya/config/config.cfg

name=$(uname -n)
echo Current user is: $name
if  [[ $name == "x230" ]]; then
    playlistfolder=$HOME/sync/oma/scripts/x230mya/playlists/*
elif [[ $name == "elementary-ThinkPad-W520" ]]; then
    playlistfolder=$HOME/sync/oma/scripts/w520mya/playlists/*
else
    playlistfolder=$playlistfolder
fi

historyviewer=$historyviewer
playliststart=$playliststart

# List files and select from list
PS3="Your choice: "
select FILENAME in $playlistfolder;
do
    case $FILENAME in
        *)
            echo -e "You picked \e[1;31m$FILENAME\e[0m"
            $historyviewer $FILENAME playlist
            break
            ;;
    esac
done
$playliststart
