#!/bin/bash

source $HOME/.mya/config/config.cfg

name=$(uname -n)
echo Current user is: $name


if  [[ $name == "x230" ]]; then
    honsavefile=$HOME/sync/oma/scripts/x230mya/offlinehistory.txt
    finalfolder=$HOME/hon/mp4/finals
    finalfolder2=$HOME/hon/mp4/finals/*
    filefolder=$HOME/hon/mp4/*
elif [[ $name == "elementary-ThinkPad-W520" ]]; then
    honsavefile=$HOME/sync/oma/scripts/w520mya/offlinehistory.txt
    finalfolder=$HOME/hdd/hon/finals
    finalfolder2=$HOME/hdd/hon/finals/*
    filefolder=$HOME/hdd/hon/*
else
    honsavefile=$honsavefile
    finalfolder=$finalfolder
    finalfolder2=$finalfolder2
    filefolder=$filefolder
fi

startmenu=$startmenu

# List files and select from list
echo "Type number of the file you want to watch, anything else to return to main menu"
PS3="Your choice: "
select FILENAME in $filefolder;
do
    case $FILENAME in
        $finalfolder)
            select FILENAME2 in $finalfolder2;
            do
            echo -e "You picked \e[1;31m$FILENAME2\e[0m"
            echo "$FILENAME2" >> $honsavefile
            mpv "$FILENAME2"
            done
            ;;
        *)
            if [[ $FILENAME == "" ]]; then
                $startmenu
            fi
            echo -e "You picked \e[1;31m$FILENAME\e[0m"
            echo "$FILENAME" >> $honsavefile
            mpv "$FILENAME"
            ;;
    esac
done
