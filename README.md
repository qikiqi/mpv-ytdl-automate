# # README # #

Few BASH scripts I created to make watching and downloading various content easier and smarter. Utilizes mpv and youtube-dl.

## v 0.5 (14/04/2016) ##

### Installer ###
    - Created an installer that helps you install mpv-ytdl-automate and create function / alias for ease of use
### Honrenamer ###
    - Added automated renumbering so historytool wont complain
### Massdownloader ###
    - Added this tool to download the material you want and where you want to download it


## v 0.4 (07/04/2016) ##

### Settings ###
    - Disable / Enable usage tracking
    - Changing the format of honrenamers extension to change
    - Changing the amount of characters to remove with honrenamer
    - Changing the timeout of hondownloader
    - Changing the current show to watch with /a/
### Honplayer ###
    - Added possibility to return to main menu by typing anything else than valid number
### Serieswatcher ###
    - Possibility to change series folder through settings
### Hondownloader ###
    - Possibility to change timeout through settings
### Honrenamer ###
    - You can now remove the certain amount of characters with this tool, changes via settings
    - Has also possibility to limit your renaming to certain filetype, changes via settings
### Historytool ###
    - New folder structure for ease of use
    - Added implementation to handle playlists
### Startmenu ###
    - Tracks how many times you have used script
    - New keyboard selection map
    - Added the new tool Playlists
### Playlists ###
    - Now you can crate your own playlists from youtube links or from history
    - Added the option to remove created playlists
    - You can also watch created playlists


## v 0.3 (26/03/2016) ##

### History ###
    - Reworked interface
    - Improved savefile locations
### Honplayer ###
    - Reformatted the whole code
### List-format ###
    - With this tool you can list all the formats from selected Youtube video or any other video/audio that is supported by youtube-dl
### Startmenu ###
    - Added new functionalities
    - Improved interface even more to support added features and still look good
### Prompt ###
    - New way to get best format, much better and clearer
    - Cleaned the Twitch part of the code
### Overall ###
    - Added more comments
    - Cleaned the code
    - Better way to get homefolder


## v 0.2 (12/03/2016) ##

### History ###
    - Added historytool, tracks what you watch. Lets you browse your history and watch straight from history.
    - Remade viewing of the last watched file with historytool
### Hondownloader ###
    - Downloads videos from Honcast youtube channel in a way that makes watching whole seasons easier.
       - Downloads and renames from specific video to the newest video in reverse order (via youtube-dl)
### Folderplaylist (/a/) ###
    - Added functionality that aims to make the watching of whole seasons easy (via mpv)
### Improved interface ###
    - Created a new interface that aims to be more modular and easier to maintain


## v 0.1 (11/02/2016) ##

### Online ###
    - Youtube (via mpv + youtube-dl)
    - Twitch (via mpv + youtube-dl)
### Offline ###
    - Any folder with files supported by mpv
### History (limited) ###
    - Remembers what you watched last time and continues playing that file. (Also supports saving state via mpv)