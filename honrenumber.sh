#!/bin/bash

# TODO Possibility to change numbering??

source $HOME/.mya/config/config.cfg

name=$(uname -n)
echo Current user is: $name


if  [[ $name == "x230" ]]; then
    filefolder2=$HOME/hon/
elif [[ $name == "elementary-ThinkPad-W520" ]]; then
    filefolder2=$HOME/hdd/hon/
else
    filefolder2=$filefolder2
fi

startmenu=$HOME/sync/oma/scripts/mpv-ytdl-automate/startmenu.sh

numbering="0000"

for file in $filefolder2/*.webm
do
    numbering=$((numbering+1))
    num=${#filefolder2}
    num=$((num+2))
    file=$(echo $file | cut -c $num-)
    if [[ $numbering -gt "9" ]]; then
        newname=00$numbering-$file
    else
        newname=000$numbering-$file
    fi
    mv "$filefolder2$file" "$filefolder2$newname"
done
echo "Renumbering done!"
$startmenu
