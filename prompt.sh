#!/bin/bash

# This is a script that helps you to watch various online content.

# TODO Formatting + comments
# TODO Make possible that user can input multiple youtube videos and they will be played as playlist
# TODO Make own history for Twitch streams and the time they were being watched
# TODO This actually can take playlist/channels as input but saving them to history displays them weird (good but formatting needs to be worked)

source $HOME/.mya/config/config.cfg

name=$(uname -n)
echo Current user is: $name
if  [[ $name == "x230" ]]; then
    savefile=$HOME/sync/oma/scripts/x230mya/onlinehistory.txt
    twitchsavefile=$HOME/sync/oma/scripts/x230mya/twitchhistory.txt
    honplayer=$HOME/sync/oma/scripts/mpv-ytdl-automate/honplayer.sh
elif [[ $name == "elementary-ThinkPad-W520" ]]; then
    savefile=$HOME/sync/oma/scripts/w520mya/onlinehistory.txt
    twitchsavefile=$HOME/sync/oma/scripts/w520mya/twitchhistory.txt
    honplayer=$HOME/sync/oma/scripts/mpv-ytdl-automate/honplayer.sh
else
    savefile=$savefile
    twitchsavefile=$twitchsavefile
    honplayer=$honplayer
fi

echo -n -e " Youtube \e[1;31m[1]\e[0m or Twitch \e[1;31m[2]\e[0m"
read -n 1 type
echo
if [[ $type == 1 ]]; then
    echo -n " Youtube URL: "
    read URL
    # Select best possible stream quality
    mpv --ytdl-format=bestvideo[ext=mp4]+bestaudio/bestvideo[ext=mp4]+bestaudio[ext=webm]/bestvideo[ext=webm]+bestaudio[ext=webm]/best[ext=mp4]/best "$URL"
    urlclean=$(youtube-dl -e $URL | tr '|' ' ' | tr '{' ' ' | tr '*' ' ')
    echo mpv --ytdl-format=bestvideo[ext=mp4]+bestaudio/bestvideo[ext=mp4]+bestaudio[ext=webm]/bestvideo[ext=webm]+bestaudio[ext=webm]/best[ext=mp4]/best "$URL" \| $urlclean \| $(date) >> $savefile
    exit 1
else
    echo -n " Streamers name: "
    read name
    echo -n -e " Quality \e[1;31m[Medium = 1, High = 2, Best = 3]\e[0m: "
    read -n 1 quality
    echo
    if  [ "$quality" == 1 ]; then
        # Stream quality Medium and name
        mpv --ytdl-format Medium http://www.twitch.tv/$name
        echo mpv --ytdl-format Medium http://www.twitch.tv/$name >> $twitchsavefile
        exit 1
    elif [ "$quality" == 2 ]; then
        # Stream quality High and name
        mpv --ytdl-format High http://www.twitch.tv/$name
        echo mpv --ytdl-format High http://www.twitch.tv/$name >> $twitchsavefile
        exit 1
    else
        # Best quality and name
        mpv -ytdl http://www.twitch.tv/$name
        echo mpv -ytdl http://www.twitch.tv/$name >> $twitchsavefile
        exit 1
    fi
fi

exit 1
