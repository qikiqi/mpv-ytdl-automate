#!/bin/bash

# TODO timeout is very sensitive about its timing so needs better way to determine positon of video
# TODO Enable / Disable autonumbering in hondownloader > Historytool depends on the autonumbering, if that can be turned off, create new method to historytool
source $HOME/.mya/config/config.cfg

name=$(uname -n)
echo Current user is: $name
echo -e "\e[1;31mThis script easily allows you to download new Honcast videos \e[0m"
echo "If this script doesn't work, try to change timeout from settings"
if  [[ $name == "x230" ]]; then
    filefolder=$HOME/hon/
    timeoutfile=$HOME/sync/oma/scripts/x230mya/settings/hondownloadertimeoutfile.txt
elif [[ $name == "elementary-ThinkPad-W520" ]]; then
    filefolder=$HOME/hdd/hon/
    timeoutfile=$HOME/sync/oma/scripts/w520mya/settings/hondownloadertimeoutfile.txt
else
    filefolder2=$filefolder2
    timeoutfile=$timeoutfile
fi

curtimeout=$(cat $timeoutfile)

echo -n -e "Type the URL of the video which is the last one to download then press \e[1;31m[ENTER]\e[0m: "
read ID
echo
#TODO If url is already in right format, do nothing. Can be checked with

tocut=${#ID}

if  [[ $tocut != 11 ]]; then
    num=$((tocut-11))
    ID=${ID:$num}
fi

nth=$(timeout $curtimeout youtube-dl --list-format https://www.youtube.com/user/honcast/videos | grep -B 1 $ID)
nth=${nth:29}
nth=$(echo $nth | sed 's/\s.*$//')

# TODO Automatically search based on the string of the video whats the index of download
# TODO Static format? ALSO needs better way to determine format
cd $filefolder2
youtube-dl --playlist-end $nth --playlist-reverse -o "%(autonumber)s-%(title)s.%(ext)s" --no-continue -f 302+251 --prefer-ffmpeg https://www.youtube.com/user/honcast/videos
