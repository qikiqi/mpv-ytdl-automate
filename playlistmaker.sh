#!/bin/bash

# First we test which computer is using this script so we can determine the right folders for history, content etc.

source $HOME/.mya/config/config.cfg

name=$(uname -n)
echo Current user is: $name
if  [[ $name == "x230" ]]; then
    playlistfolder2=$HOME/sync/oma/scripts/x230mya/playlists/
    onlinehistory=$HOME/sync/oma/scripts/x230mya/onlinehistory.txt
    onlinehistorycolourfinal=$HOME/sync/oma/scripts/x230mya/finalonlinehistorycolour.txt
elif [[ $name == "elementary-ThinkPad-W520" ]]; then
    playlistfolder2=$HOME/sync/oma/scripts/w520mya/playlists/
    onlinehistory=$HOME/sync/oma/scripts/w520mya/onlinehistory.txt
    onlinehistorycolourfinal=$HOME/sync/oma/scripts/w520mya/finalonlinehistorycolour.txt
else
    playlistfolder2=$playlistfolder2
    onlinehistory=$onlinehistory
    onlinehistorycolourfinal=$onlinehistorycolourfinal
fi

playliststart=$playliststart

# Numberdetectorhelper
re='^[0-9]+$'

echo "Hello! Today we will be making a new playlist for you"
# Create textfile for playlist
while true; do
    echo -n -e "Name of the playlist then \e[1;31m[ENTER]\e[0m"
    read name
    echo $name
        if [[ "$name" =~ \ |\' ]]; then
            echo "Name contains illegal characters, please remove them and try again"
        elif [[ -e "$playlistfolder2/$name.txt" ]]; then
            echo "File already exists"
            echo "Modifying the file"
            break
        else
            touch $playlistfolder2/$name.txt
            break
        fi
done


# Show user the onlinehistory
nl $onlinehistorycolourfinal
nmofrows=$(wc -l < $onlinehistorycolourfinal)
num=$((nmofrows-1))


while true; do

    echo -n -e "Select the item to be added by typing its number (q to quit) and then press \e[1;31m[ENTER]\e[0m"
    echo -e "\nYou can also input Youtube links"
# TODO SOMEWHERE HERE IS BUG WITH DEALING YOUTUBE-LINKS AND NUMBERS AT THE SAME TIME
    read input
    if [[ "$input" == *";"* ]]; then
        arr=$(echo $input | tr ";" "\n")
        for x in $arr
        do
            if [[ "$x" == *"youtu"* ]]; then
                # Determine if input contains *yout*
                urlclean=$(youtube-dl -e $x | tr '|' ' ' | tr '{' ' ' | tr '*' ' ') 
                echo mpv --ytdl-format=bestvideo[ext=mp4]+bestaudio/bestvideo[ext=mp4]+bestaudio[ext=webm]/bestvideo[ext=webm]+bestaudio[ext=webm]/best[ext=mp4]/best "$input" \| $urlclean \| $(date) >> $playlistfolder2/$name.txt
                echo "Added!"
            elif [[ $x -gt $num ]]; then
                echo "Your input is greater than the amount of rows in historyfile, try again."
            elif [[ $x =~ $re ]]; then
                # Add from NL'ed list
                linetoadd=$(sed "${x}q;d" $onlinehistory)
                echo $linetoadd >> $playlistfolder2/$name.txt
                echo "Added!"
            fi
        done
        break
    else
        if [[ "$input" == *"youtu"* ]]; then
            # Determine if input contains *yout*
            urlclean=$(youtube-dl -e $input | tr '|' ' ' | tr '{' ' ' | tr '*' ' ') 
            echo mpv --ytdl-format=bestvideo[ext=mp4]+bestaudio/bestvideo[ext=mp4]+bestaudio[ext=webm]/bestvideo[ext=webm]+bestaudio[ext=webm]/best[ext=mp4]/best "$input" \| $urlclean \| $(date) >> $playlistfolder2/$name.txt
            echo "Added!"
        elif [[ $input -gt $num ]]; then
            echo "Your input is greater than the amount of rows in historyfile, try again."
        elif [[ $input =~ $re ]]; then
         # Add from NL'ed list
            linetoadd=$(sed "${input}q;d" $onlinehistory)
            echo $linetoadd >> $playlistfolder2/$name.txt
            echo "Added!"
        elif [[ "$input" == "q" ]]; then
            # q to quit while loop    
            break
        fi
    fi
done
$playliststart
